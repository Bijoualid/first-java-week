package co.simplon.promo16.bases;

import java.util.ArrayList;
import java.util.List;

public class Variables {
    
    public void first() {
        int age = 27;
        String namePromo = "#Promo16";
        Boolean trueOrFlase = true;
        List<String>firstNamesList = new ArrayList<>(List.of("HTML","CSS","Bootstrap","JavaScript",
        "PHP","JAVA"));

        System.out.println(age);
        System.out.println(namePromo);
        System.out.println(trueOrFlase);
        System.out.println(firstNamesList);
    }


    public void withParameters(String name, int age) {
        System.out.println(name + " " + age);
    }


    public String withReturn() {
        String sentence = "Ceci est une révision";
        System.out.println(sentence);

        return sentence;
    }
}
